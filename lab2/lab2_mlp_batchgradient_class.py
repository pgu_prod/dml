﻿# -*- coding: utf-8 -*-
"""
Created on Thu Mar 11 20:29:37 2021

@author: AM4
"""
import random
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder

df = pd.read_csv('data.csv')
data = df.sample(n=100)

X = data.iloc[:, :4].values
y = OneHotEncoder(sparse=False).fit_transform( data.iloc[:, 4].values.reshape(-1, 1))

# Получить список имен столбцов в преобразованном наборе данных
# column_names = OneHotEncoder(sparse=False).fit(y).get_feature_names_out()
# print(column_names)
# так как ответы у нас строки - нужно перейти к численным значениям
print(np.unique(y))
print(y[:10,:])

# зададим функцию активации - сигмоида
def sigmoid(y):
    return 1 / (1 + np.exp(-y))


# нам понадобится производная от сигмоиды при вычислении градиента
def derivative_sigmoid(y):
    return sigmoid(y) * (1 - sigmoid(y))


# инициализируем нейронную сеть 
inputSize = X.shape[1]  # количество входных сигналов равно количеству признаков задачи
hiddenSizes = 5  # задаем число нейронов скрытого слоя
outputSize = y.shape[1]  # количество выходных сигналов равно количеству классов задачи


# прямой проход
def feed_forward(x):
    input_ = x  # входные сигналы
    hidden_ = sigmoid(np.dot(input_, weights[0]))  # выход скрытого слоя = сигмоида(входные сигналы*веса скрытого слоя)
    output_ = sigmoid(
        np.dot(hidden_, weights[1]))  # выход сети (последнего слоя) = сигмоида(выход скрытого слоя*веса выходного слоя)

    # возвращаем все выходы, они нам понадобятся при обратном проходе
    return [input_, hidden_, output_]



# на вход принимает скорость обучения, реальные ответы, предсказанные сетью ответы и выходы всех слоев после прямого прохода
def backward(learning_rate, target, net_output, layers, mode):
    # считаем производную ошибки сети
    err = (target - net_output)

    # прогоняем производную ошибки обратно ко входу, считая градиенты и корректируя веса
    # для этого используем chain rule
    # цикл перебирает слои от последнего к первому
    for i in range(len(layers) - 1, 0, -1):
        # градиент слоя = ошибка слоя * производную функции активации * на входные сигналы слоя

        # ошибка слоя * производную функции активации
        err_delta = err * derivative_sigmoid(layers[i])

        # пробрасываем ошибку на предыдущий слой
        err = np.dot(err_delta, weights[i - 1].T)

        # ошибка слоя * производную функции активации * на входные сигналы слоя
        if mode == 0:
            dw = np.dot(layers[i - 1].T, err_delta)
        if mode == 1:
            dw = np.dot(layers[i - 1][:, np.newaxis], err_delta[np.newaxis, :])

        # обновляем веса слоя
        weights[i - 1] += learning_rate * dw


# функция обучения чередует прямой и обратный проход
def train(x_values, target, learning_rate, mode):
    if mode == 0:
        output = feed_forward(x_values)
        backward(learning_rate, target, output[2], output, mode)
        out = np.amax(output[2], axis=1)
    if mode == 1:
        index = np.arange(len(x_values))
        random.shuffle(index)
        for i in range(len(x_values)):
            output = feed_forward(x_values[index[i]])
            backward(learning_rate, target[index[i]], output[2], output, mode)

    return None


# функция предсказания возвращает только выход последнего слоя
def predict(x_values):
    output = feed_forward(x_values)[-1]
    result = np.where(np.amax(output, axis=1).reshape(-1, 1) == output, 1, 0)
    return result


# задаем параметры обучения
iterations = 5000
learning_rate = 0.01
stopLearn = False

# обучаем сеть (фактически сеть это вектор весов weights)

def trainModel(X, y, mode):
    if mode == 0:
        print("Прямой спуск")
    if mode == 1:
        print("Стохастический спуск")

    r3 = [1000, 1000, 1000]
    for i in range(iterations):
        train(X, y, learning_rate, 0)

        if i % 50 == 0:
            # считаем ошибку на обучающей выборке
            pr = predict(X)

            # считаем ошибку на всей выборке
            y1 = np.where(df.iloc[:, 4].values == "Iris-setosa", 1, 0).reshape(-1, 1)
            X1 = df.iloc[:, :4].values
            r1 = sum(abs(y - (pr > 0.5)))
            r2 = sum(abs(y1 - (predict(X1) > 0.5)))
            print("На итерации: " + str(i) + ' || ' + "Средняя ошибка: "
                  + str(np.mean(np.square(y - predict(X)))) + '\t: ' + str(r1)
                  + ' : ' + str(r2))

            if all(r2 < r3):
                r3 = r2
            if np.all(r1 == 0) and np.all(r2 == 0) and stopLearn:
                break

weights = [
    np.random.uniform(-2, 2, size=(inputSize, hiddenSizes)),  # веса скрытого слоя
    np.random.uniform(-2, 2, size=(hiddenSizes, outputSize))  # веса выходного слоя
]
#
# trainModel(X, y, 1)
#
weights = [
    np.random.uniform(-2, 2, size=(inputSize, hiddenSizes)),  # веса скрытого слоя
    np.random.uniform(-2, 2, size=(hiddenSizes, outputSize))  # веса выходного слоя
]
trainModel(X, y, 0)

# output = predict(X[0])
# print(output)